const gulp = require('gulp');
const watch = require('gulp-watch');
const spawn = require('child_process').spawn; //Sync;
const exec = require('child_process').exec;
const fs = require('fs');
const StreamSplitter = require("stream-splitter");

//catch first real arg and trim -
const prefix = process.argv.length > 3 ? process.argv[3].split('-').join('') + '/' || './' : './';

const rerunpattern = /Rerun/g;

try {
	process.chdir(prefix);
} catch (err) {
	console.error(err);
}

const Params = ['-c-style-errors', '-aux-directory=' + 'temp', '-output-directory=' + 'out'];
const texFiles = fs.readdirSync('.').filter(function (name) { return name.endsWith('.tex') });


var PDFInstance = undefined;

var ltxOps = {
	rerun: false,
	runs: 0,
	success: false
};

//process.stdin.resume();
process.stdin.setEncoding('utf8');

function spawnTex(target) {
	ltxOps.runs++;
	var latex = spawn('pdflatex', Params.concat([target + '.tex']), { stdio: [process.stdin, 'pipe', process.stderr], cwd: process.cwd() });
	//split stream?
	latex.stdout.pipe(process.stdout);
	var splitter = latex.stdout.pipe(StreamSplitter('\n'));
	//tokens as strings
	splitter.encoding = "utf8";
	//show output
	splitter.on("token", function(token) {
		//one line of input
		if (token.search(rerunpattern) > 0) {
			ltxOps.rerun = true;
		}
	});
	return latex;
}

function spawnPDF(target) {
	return exec('start ' + 'out/' + target + '.pdf', [], {cwd: process.cwd()});
}

//checks if directory is accessible/exists and trys to create it if the check fails
function checkDir(dir) {
	fs.access(dir, fs.F_OK, (err) => {
		if (err)
			fs.mkdirSync(dir);
	});
}

gulp.task('clean', function () {
	if (PDFInstance !== undefined) {
		PDFInstance.kill('SIGINT');
	}

	console.log("All clean");
});

texFiles.forEach(function (taskName) {
	taskName = taskName.split('.')[0];
	gulp.task(taskName, ['clean'], function (cb) {
		console.log("Running LaTeX");
		var compile = function () {
			ltxOps.runs++;
			const latex = spawnTex(taskName);

			latex.on('exit', (code) => {
				console.log(`Child exited with code ${code}`);
				if (!ltxOps.rerun) {
					ltxOps.success = true;
					console.log('View PDF');
					PDFInstance = spawnPDF(taskName);
				} else if (ltxOps.rerun && ltxOps.runs < 5 && !ltxOps.success) {
					compile();
				} else {
					console.error("reached rerun limit");
					cb();
				}
			});
		};
		compile();
	});

	gulp.task(taskName + '-watch', [taskName], function () {
		watch('**/*.tex', ['clean', taskName]);
	});

	console.log("created task: " + taskName);
});

//run init with -.. for upper folder
gulp.task('init', function () {
	console.log("Running INIT task in: " + prefix);
	checkDir('out');
	checkDir('temp');
});

gulp.task('default', function () {
	console.log("Nothing to do");
});
